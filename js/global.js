$(document).ready(function() {
    var prevarrow = '<div class="slick-prev-arrow"> <i class="fas fa-chevron-left"></i> </div>';
    var nextArrow = '<div class="slick-next-arrow"> <i class="fas fa-chevron-right"></i> </div>';

    $(".filter-item-title.closed").parent().find(".filter-item-checkbox").slideUp(600);

    //Header MENU

    $(".header-mobile-search").on('click', function () {
        var menu = $(this).parents(".row").find(".header-main-item.search");

        if($(this).hasClass('opened')){
            $(menu).slideUp(300);
            $(this).removeClass('opened');
        } else if(!$(this).hasClass('opened')){
            $(this).addClass('opened');
            $(menu).slideDown(300);
        }
    });

    $(".header-mobile-filter").on('click', function () {
        var menu = $(this).parent().find(".header-filter-container");

        if($(this).hasClass('opened')){
            $(menu).slideUp(300);
            $(this).removeClass('opened');
        } else if(!$(this).hasClass('opened')){
            $(this).addClass('opened');
            $(menu).slideDown(300);
        }
    });

    $(".header-mobile-nav").on('click', function () {
        var menu = $(this).parent().find(".header-nav-menu");

        if($(this).hasClass('opened')){
           $(menu).slideUp(300);
           $(this).removeClass('opened');
        } else if(!$(this).hasClass('opened')){
            $(this).addClass('opened');
            $(menu).slideDown(300);
        }
    });

    $(".header-nav-menu-item .fa-chevron-down").on('click', function () {
        var parent = $(this).parent().parent(".header-nav-menu-item");
        var content = $(parent).find(".header-nav-menu-item-info");

        if ($(window).width() >= 992) {
            if (!$(parent).hasClass('opened')) {
                $(parent).addClass("opened");
                $(content).slideDown(300);
            } else if ($(parent).hasClass('opened')) {
                $(parent).removeClass("opened");
                $(content).slideUp(300);
            }
        } else {
            return 0;
        }
    });

    //Footer MENU

    $(".footer-content.toggle").on('click', function () {
        var menu = $(this).find(".toggle-item");
        var plus = $(this).find('fa-plus');
        var minus = $(this).find('fa-minus');

        if ($(window).width() <= 1200){
            if($(this).hasClass('opened')){
                $(menu).slideUp(300);
                $(this).removeClass('opened');
            } else if(!$(this).hasClass('opened')){
                $(".toggle-item").slideUp(300);
                $(".footer-content.toggle").removeClass('opened');
                $(this).addClass('opened');
                $(menu).slideDown(300);
            }
        } else {
            return 0;
        }
    });

    //Go TOP
    $(window).scroll(function () {
        if ($(this).scrollTop() > 400) {
            $('.go_top_block').fadeIn();
        } else {
            $('.go_top_block').fadeOut();
        }
    });

    $('.go_top').on('click', function(){
        $('html, body').animate({ scrollTop:0 }, 1500);
    });

    //Filter

    $(".product-content-toggle").on('click', function () {
        event.preventDefault();

        if($(this).data("id") === 'table'){
           $("#product-toggle-content").removeClass("string");
       } else if($(this).data("id") === "string"){
           $("#product-toggle-content").addClass("string");
       }
    });

    $(".product-mobile-filter").on('click', function () {
        var menu = $(this).parent().find(".product-filter-container");

        if($(this).hasClass('opened')){
            $(menu).slideUp(300);
            $(this).removeClass('opened');
        } else if(!$(this).hasClass('opened')){
            $(this).addClass('opened');
            $(menu).slideDown(300);
        }
    });

    $(".filter-item-title").on('click', function () {
        var content = $(this).parent().find(".filter-item-checkbox");

        if(!$(this).hasClass("closed")){
            $(this).addClass("closed");
            $(content).slideUp(600);
        } else if ($(this).hasClass("closed")){
            $(this).removeClass("closed");
            $(content).slideDown(600);
        }
    });

    //Sliders

    if($("body").hasClass("main")){
        $('.main-slider .slick-slider').slick({
            dots: true,
            arrows: true,
            prevArrow: ('<div class="main-slider-slick_arrow prev d-none d-xl-flex"><i class="fas fa-chevron-left"></i></div>'),
            nextArrow: ('<div class="main-slider-slick_arrow next d-none d-xl-flex "><i class="fas fa-chevron-right"></i></div>'),
            slidesToShow: 1,
            slidesToScroll: 1
        });
    }

    $('.product-slider .slick-slider').slick({
        dots: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: ('<div class="slick_arrow prev"><i class="fas fa-chevron-left"></i></div>'),
        nextArrow: ('<div class="slick_arrow next"><i class="fas fa-chevron-right"></i></div>'),
        responsive: [
            {
                breakpoint: 1199.98,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 991.98,
                settings: {
                    slidesToShow: 2
                }
            },
                {
                breakpoint: 767.98,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });

    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: ('<div class="slick_arrow prev"><i class="fas fa-chevron-left"></i></div>'),
        nextArrow: ('<div class="slick_arrow next"><i class="fas fa-chevron-right"></i></div>'),
        asNavFor: '.slider-for',
        focusOnSelect: true
    });
});
